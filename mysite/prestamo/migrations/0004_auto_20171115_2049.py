# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-15 20:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prestamo', '0003_auto_20171115_2028'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='title',
            new_name='email',
        ),
        migrations.RemoveField(
            model_name='post',
            name='text',
        ),
        migrations.AddField(
            model_name='post',
            name='dni',
            field=models.CharField(default=36435234, max_length=8),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='genero',
            field=models.CharField(default='M', max_length=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='monto',
            field=models.CharField(default='324.25', max_length=200),
            preserve_default=False,
        ),
    ]
