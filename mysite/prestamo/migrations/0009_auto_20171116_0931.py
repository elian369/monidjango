# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-16 09:31
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prestamo', '0008_auto_20171116_0920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='dni',
            field=models.CharField(max_length=8, validators=[django.core.validators.RegexValidator(b'^[0-9]*$', b'Only numeric characters are allowed.')]),
        ),
        migrations.AlterField(
            model_name='post',
            name='genero',
            field=models.CharField(choices=[(b'M', b'Male'), (b'F', b'Female')], max_length=1),
        ),
    ]
