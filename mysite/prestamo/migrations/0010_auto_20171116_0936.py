# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-16 09:36
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prestamo', '0009_auto_20171116_0931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='monto',
            field=models.CharField(max_length=200, validators=[django.core.validators.RegexValidator(b'^[0-9]*$', b'Only numeric characters are allowed.')]),
        ),
        migrations.AlterField(
            model_name='post',
            name='name',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator(b'^[a-zA-Z]*$', b'Only alphabetic characters are allowed.')]),
        ),
        migrations.AlterField(
            model_name='post',
            name='surname',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator(b'^[a-zA-Z]*$', b'Only alphabetic characters are allowed.')]),
        ),
    ]
