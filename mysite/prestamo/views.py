# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.utils import timezone
from .models import Post
from django.shortcuts import render, get_object_or_404
from .forms import PostForm
from .forms import LoginForm
from django.shortcuts import redirect
import json
import urllib2
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

from django.contrib.auth import logout


def logout_page(request):
    logout(request)
    return redirect('post_new')
    

def login_page(request):
    #logout(request)
    message = None
    if(request.method == "POST"):
        form = LoginForm(request.POST)
        if(form.is_valid()):
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if(user is not None):
                login(request, user)
                message = "login exitoso"
		posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
		return render(request, 'prestamo/post_list.html', {'posts': posts})		
            else:
                message = "usuario inactivo"
        else:
            message = "usuario y/o password incorrecto/s"
    else:
        form = LoginForm()
    return render(request, 'prestamo/login.html', {'message' : message, 'form' : form})

@login_required
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'prestamo/post_list.html', {'posts': posts})

  
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    estado = decidirAprobacion(post)
    return render(request, 'prestamo/post_detail.html', {'post': post, 'desicion': estado})
        
    #aca ver si esta aprovado o no dependiendo lo que me devuelva la api
    #http://scoringservice.moni.com.ar:7001/api/v1/scoring/?document_number=30156149&gender=M&email=fran@mail.com
    


def decidirAprobacion(post):
    url = "http://scoringservice.moni.com.ar:7001/api/v1/scoring/?document_number="+post.dni+"&gender="+post.genero+"&email="+post.email 
    response = urllib2.urlopen(url)
    data = json.load(response)
    if((data['error']) or (not(data['approved']))):
        return("NoAprobado")
    return("Aprobado")



def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            #print form.cleaned_data['name']
            post = form.save(commit=False)
            #post.author = request.user
            post.published_date = timezone.now()
            #post.decision = "aprobado"
            post.save()
            estado = decidirAprobacion(post)
            return render(request, 'prestamo/post_edit.html', {'form': form, 'desicion':estado})
    else:
        form = PostForm()
    
    return render(request, 'prestamo/post_edit.html', {'form': form})



def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            print form.cleaned_data['monto']
            post = form.save(commit=False)
            post.author = request.user
            #post.decision = "aprobado"
            post.save()
            estado = decidirAprobacion(post)
            return render(request, 'prestamo/post_edit.html', {'form': form, 'desicion':estado})
    else:
        form = PostForm(instance=post)
    return render(request, 'prestamo/post_edit.html', {'form': form})

