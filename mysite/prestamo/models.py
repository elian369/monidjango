from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator

class Post(models.Model):

    numeric = RegexValidator(r'^[0-9]*$', 'Only numeric characters are allowed.')
    alphabetic = RegexValidator(r'^[a-zA-Z]*$', 'Only alphabetic characters are allowed.')


    #title = models.CharField(max_length=200)
    name = models.CharField(max_length = 30, validators=[alphabetic])
    surname = models.CharField(max_length = 30, validators=[alphabetic])
    dni = models.CharField(max_length = 8, validators=[numeric])
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    genero = models.CharField(max_length=1, choices=GENDER_CHOICES)
    #genero = models.CharField(max_length = 1)
    email = models.CharField(max_length = 200)
    monto = models.CharField(max_length = 200, validators=[numeric])
    desicion = models.CharField(max_length = 200)
    #author = models.ForeignKey('auth.User')
    #text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
