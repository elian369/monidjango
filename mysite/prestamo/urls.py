from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^list/$', views.post_list),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^$', views.post_new, name='post_new'),
    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
    url(r'^accounts/login/$', views.login_page, name='login_page'),
    url(r'^logout/$', views.logout_page, name='logout_page')
]
